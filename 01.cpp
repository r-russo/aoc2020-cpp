#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

bool is_numeric(const std::string &num) {
    return std::all_of(num.begin(), num.end(), isdigit);
}

int main() {
    std::ifstream f;
    std::vector<int> in;
    std::string line;

    f.open("../input_01.txt");

    if (!f.is_open()) {
        return 1;
    }

    while (std::getline(f, line)) {
        if (is_numeric(line)) {
            in.push_back(std::stol(line));
        }
    }

    std::sort(in.begin(), in.end(), std::less<>());

    // part one
    int sum {0}, i {0};
    while (sum != 2020 && i < in.size()) {
        for (int j = i + 1; j < in.size(); j++) {
            sum = in[i] + in[j];
            if (sum == 2020) {
                std::cout << in[i] * in[j] << std::endl;
                break;
            } else if (sum > 2020) {
                break;
            }
        }
        i++;
    }

    // part two
    i = 0;
    int j = 0;
    sum = 0;
    while (sum != 2020 && i < in.size()) {
        while (sum != 2020 && j < in.size()) {
            for (int k = j + 1; k < in.size(); k++) {
                sum = in[i] + in[j] + in[k];
                if (sum == 2020) {
                    std::cout << in[i] * in[j] * in[k] << std::endl;
                    break;
                } else if (sum > 2020) {
                    break;
                }
            }
            j++;
        }
        i++;
    }

    f.close();
    return 0;
}