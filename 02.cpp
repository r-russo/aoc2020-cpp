#include <iostream>
#include <fstream>
#include <string>

int main() {
    std::ifstream f;
    std::string line;

    f.open("../input_02.txt");

    if (!f.is_open()) {
        return 1;
    }

    // part one
    int count_valid = 0;
    while (std::getline(f, line)) {
        auto find_pos = line.find(':');
        std::string condition = line.substr(0, find_pos);
        std::string password = line.substr(find_pos + 2, std::string::npos);

        find_pos = condition.find(' ');
        std::string counts = condition.substr(0, find_pos);
        char letter = condition.at(find_pos + 1);

        find_pos = counts.find('-');
        auto min = std::stol(counts.substr(0, find_pos));
        auto max = std::stol(counts.substr(find_pos + 1, std::string::npos));

        int count_char = 0;

        for (const char &c: password) {
            if (c == letter) {
                count_char++;
            }

            if (count_char > max) {
                break;
            }
        }

        if (count_char <= max && count_char >= min) {
            count_valid++;
        }
    }

    std::cout << count_valid << std::endl;

    // part two
    count_valid = 0;
    f.clear();
    f.seekg(0);
    while (std::getline(f, line)) {
        auto find_pos = line.find(':');
        std::string condition = line.substr(0, find_pos);
        std::string password = line.substr(find_pos + 2, std::string::npos);

        find_pos = condition.find(' ');
        std::string counts = condition.substr(0, find_pos);
        char letter = condition.at(find_pos + 1);

        find_pos = counts.find('-');
        auto pos1 = std::stol(counts.substr(0, find_pos));
        auto pos2 = std::stol(counts.substr(find_pos + 1, std::string::npos));

        if ((letter == password.at(pos1 - 1)) != (letter == password.at(pos2 - 1))) {
            count_valid++;
        }
    }

    std::cout << count_valid << std::endl;

    return 0;
}