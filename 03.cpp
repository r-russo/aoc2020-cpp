#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int main() {
    std::ifstream f;
    f.open("../input_03.txt");

    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    int num_line = 0;
    std::vector<std::vector<bool>> map;
    while (std::getline(f, line)) {
        map.emplace_back();
        for (const char &c : line) {
            map[num_line].push_back(c == '.');
        }
        num_line++;
    }

    // part one
    int x {0}, trees {0};
    int line_width = map[0].size();

    for (int y = 1; y < map.size(); y++) {
        x = (x + 3) % line_width;
        if (!map[y][x]) {
            trees++;
        }
    }
    std::cout << trees << std::endl;

    // part two
    std::vector<int> trees_vec {0, 0, 0, 0, 0};

    for (int i = 0; i < trees_vec.size(); i++){
        x = 0;
        for (int y = 1; y < map.size(); y++) {
            switch(i) {
                case 0:
                    x = (x + 1) % line_width;
                    break;
                case 1:
                    x = (x + 3) % line_width;
                    break;
                case 2:
                    x = (x + 5) % line_width;
                    break;
                case 3:
                    x = (x + 7) % line_width;
                    break;
                case 4:
                    if (y % 2 != 0) {
                        continue;
                    }
                    x = (x + 1) % line_width;
                    break;
                default:
                    break;
            }

            if (!map[y][x]) {
                trees_vec[i]++;
            }
        }
    }

    uint64_t prod = 1;
    for (const auto &t : trees_vec) {
        prod *= t;
        std::cout << t << std::endl;
    }

    std::cout << prod << std::endl;

    return 0;
}