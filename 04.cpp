#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <algorithm>

bool check_value(const std::string& field, std::string value) {
    if (field == "byr") {
        auto num_val = std::stol(value);
        return (value.size() == 4 && num_val >= 1920 && num_val <= 2002);
    } else if (field == "iyr") {
        auto num_val = std::stol(value);
        return (value.size() == 4 && num_val >= 2010 && num_val <= 2020);
    } else if (field == "eyr") {
        auto num_val = std::stol(value);
        return (value.size() == 4 && num_val >= 2020 && num_val <= 2030);
    } else if (field == "hgt") {
        std::string unit = value.substr(value.size() - 2, 2);
        auto num_val = std::stol(value);
        if (unit == "cm") {
            return (num_val >= 150 && num_val <= 193);
        } else if (unit == "in") {
            return (num_val >= 59 && num_val <= 76);
        }
    } else if (field == "hcl") {
        auto hex = value.substr(1, std::string::npos);
        bool is_number = true;
        for (const char &c : hex) {
            if (!isxdigit(c)) {
                is_number = false;
                break;
            }
        }
        return (value[0] == '#' && hex.size() == 6 && is_number);
    } else if (field == "ecl") {
        return (value == "amb" || value == "blu" || value == "brn" || value == "gry" ||
                value == "grn" ||value == "hzl" || value == "oth");
    } else if (field == "pid") {
        bool is_number = true;
        for (const char &c : value) {
            if (!isdigit(c)) {
                is_number = false;
                break;
            }
        }
        return (value.size() == 9 && is_number);
    } else if (field == "cid") {
        return true;
    }

    return false;
}

int main() {
    std::ifstream f;
    f.open("../input_04.txt");

    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    const std::vector<std::string> fields {
            "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid", "cid"
    };

    uint8_t valid = 0;
    int count_valid = 0;
    std::string field, value;

    // part one
    while (std::getline(f, line)) {
        bool reading_value = false;
        if (line.empty()) {
            if (valid == 0b01111111 || valid == 0b11111111) {
                count_valid++;
            }
            valid = 0;
        } else {
            for (const char &c : line) {
                switch (c) {
                    case ':':reading_value = true;
                        break;
                    case ' ': {
                        auto it = std::find(fields.begin(), fields.end(), field);
                        if (it != fields.end()) {
                            valid |= (1 << (it - fields.begin()));
                        }
                        reading_value = false;
                        field.clear();
                        value.clear();
                        break;
                    }
                    default:
                        if (!reading_value) {
                            field += c;
                        } else {
                            value += c;
                        }
                }
            }

            auto it = std::find(fields.begin(), fields.end(), field);
            if (it != fields.end()) {
                valid |= (1 << (it - fields.begin()));
            }
            field.clear();
            value.clear();
        }
    }

    if (valid == 0b01111111 || valid == 0b11111111) {
        count_valid++;
    }

    std::cout << count_valid << std::endl;

    // part two
    valid = 0;
    count_valid = 0;
    field.clear();
    value.clear();
    f.clear();
    f.seekg(0);
    while (std::getline(f, line)) {
        bool reading_value = false;
        if (line.empty()) {
            if (valid == 0b01111111 || valid == 0b11111111) {
                count_valid++;
            }
            valid = 0;
        } else {
            for (const char &c : line) {
                switch (c) {
                    case ':':reading_value = true;
                        break;
                    case ' ': {
                        if (check_value(field, value)) {
                            auto it = std::find(fields.begin(), fields.end(), field);
                            if (it != fields.end()) {
                                valid |= (1 << (it - fields.begin()));
                            }
                        }
                        reading_value = false;
                        field.clear();
                        value.clear();
                        break;
                    }
                    default:
                        if (!reading_value) {
                            field += c;
                        } else {
                            value += c;
                        }
                }
            }

            if (check_value(field, value)) {
                auto it = std::find(fields.begin(), fields.end(), field);
                if (it != fields.end()) {
                    valid |= (1 << (it - fields.begin()));
                }
            }
            field.clear();
            value.clear();
        }
    }

    if (valid == 0b01111111 || valid == 0b11111111) {
        count_valid++;
    }

    std::cout << count_valid << std::endl;

    return 0;
}