#include <iostream>
#include <string>
#include <cmath>
#include <fstream>
#include <vector>
#include <algorithm>

int get_row(const std::string& partition) {
    int min = 0;
    int max = 127;
    for (const char &c : partition) {
        if (c == 'F') {
            max = max - (int) ceil((double) (max - min) / 2);
        } else if (c == 'B') {
            min = min + (int) ceil((double) (max - min) / 2);
        } else {
            return -1;
        }
    }

    return min;
}

int get_col(const std::string& partition) {
    int min = 0;
    int max = 7;
    for (const char &c : partition) {
        if (c == 'L') {
            max = max - (int) ceil((double) (max - min) / 2);
        } else if (c == 'R') {
            min = min + (int) ceil((double) (max - min) / 2);
        } else {
            return -1;
        }
    }

    return min;
}

int main() {
    std::ifstream f;
    f.open("../input_05.txt");
    if (!f.is_open()) {
        return 1;
    }

    // part one
    std::string seat;
    int max = 0;
    while (std::getline(f, seat)) {
        std::string row_str = seat.substr(0, 7);
        std::string col_str = seat.substr(7, 3);

        int seat_id = get_row(row_str) * 8 + get_col(col_str);

        if (seat_id > max) {
            max = seat_id;
        }
    }

    std::cout << max << std::endl;

    // part two
    f.clear();
    f.seekg(0);
    std::vector<int> seats;
    while (std::getline(f, seat)) {
        std::string row_str = seat.substr(0, 7);
        std::string col_str = seat.substr(7, 3);

        int seat_id = get_row(row_str) * 8 + get_col(col_str);

        seats.push_back(seat_id);
    }
    
    std::sort(seats.begin(), seats.end(), std::less<>());

    for (int i = 1; i < seats.size(); ++i) {
        if (seats[i] - seats[i - 1] > 1) {
            std::cout << (seats[i] + seats[i - 1]) / 2 << std::endl;
        }
    }

    return 0;
 }