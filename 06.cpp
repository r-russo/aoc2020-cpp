#include <iostream>
#include <fstream>
#include <algorithm>

int main() {
    std::ifstream f;
    f.open("../input_06.txt");
    if (!f.is_open()) {
        return 1;
    }

    // part 1
    std::string line, questions;
    int count_unique = 0;
    char last_q;
    while (std::getline(f, line)) {
        if (line.empty()) {
            std::sort(questions.begin(), questions.end(), std::less<>());
            last_q = ' ';
            for (const char &q : questions) {
                if (q != last_q) {
                    count_unique++;
                    last_q = q;
                }
            }

            questions.clear();
        } else {
            questions += line;
        }
    }

    std::sort(questions.begin(), questions.end(), std::less<>());
    last_q = ' ';
    for (const char &q : questions) {
        if (q != last_q) {
            count_unique++;
            last_q = q;
        }
    }

    std::cout << count_unique << std::endl;

    // part 2
    questions.clear();
    count_unique = 0;
    f.clear();
    f.seekg(0);
    int num_people = 0;
    while (std::getline(f, line)) {
        if (line.empty()) {
            std::sort(questions.begin(), questions.end(), std::less<>());
            last_q = ' ';

            int count_same_answer = 0;
            questions += ' ';
            for (const char &q : questions) {
                if (q != last_q) {
                    if (num_people == count_same_answer) {
                        count_unique++;
                    }
                    count_same_answer = 0;
                    last_q = q;
                }
                count_same_answer++;
            }

            questions.clear();
            num_people = 0;
        } else {
            questions += line;
            num_people++;
        }
    }

    int count_same_answer = 0;
    questions += ' ';
    for (const char &q : questions) {
        if (q != last_q) {
            if (num_people == count_same_answer) {
                count_unique++;
            }
            count_same_answer = 0;
            last_q = q;
        }
        count_same_answer++;
    }

    std::cout << count_unique << std::endl;
    return 0;
 }