#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>

struct Bag {
    std::string color {};
    int count {};
};

bool find_bag(std::map<std::string, std::vector<struct Bag>> &bags,
              const struct Bag &current_bag,
              const std::string &find_color) {
    if (current_bag.color == find_color) {
        return true;
    } else if (current_bag.count == 0) {
        return false;
    }

    for (const auto &bag: bags[current_bag.color]) {
        if (find_bag(bags, bag, find_color)) {
            return true;
        }
    }

    return false;
}

int count_bags(std::map<std::string, std::vector<struct Bag>> &bags,
               const struct Bag &current_bag) {
    if (current_bag.count == 0) {
        return 0;
    }

    int count {};
    for (auto &bag: bags[current_bag.color]) {
        count += current_bag.count * count_bags(bags, bag);
    }
    count += current_bag.count;

    return count;
}

bool read_rules(const std::string &path, std::map<std::string, std::vector<struct Bag>> &bags) {
    std::ifstream f;
    f.open(path);
    if (!f.is_open()) {
        return false;
    }

    std::string line;
    while (std::getline(f, line)) {
        size_t separator_pos = line.find("bags contain");
        std::string parent_color = line.substr(0, separator_pos - 1);

        size_t next_pos = separator_pos + 11;
        std::vector<struct Bag> current_bags;
        while (next_pos != std::string::npos) {
            struct Bag current_bag;
            size_t end = line.find(',', next_pos + 1);
            std::string inside = line.substr(next_pos + 2, end - next_pos - 1);
            next_pos = end;

            size_t first_space = inside.find(' ');
            std::string str_amount = inside.substr(0, first_space);
            if (str_amount != "no") {
                int amount = (int) std::stol(str_amount);
                size_t word_bag = inside.find(" bag");
                std::string color = inside.substr(first_space + 1, word_bag - first_space - 1);
                current_bag.color = color;
                current_bag.count = amount;
            } else {
                current_bag.color = "";
                current_bag.count = 0;
            }
            current_bags.push_back(current_bag);
        }
        bags.insert(std::pair<std::string, std::vector<struct Bag>>(parent_color, current_bags));
    }

    f.close();

    return true;
}

int main() {
    std::map<std::string, std::vector<struct Bag>> bags;
    if (!read_rules("../input_07.txt", bags)) {
        return 1;
    }

    for (const auto &bag: bags) {
        for (const auto &b: bag.second) {
            std::cout << "(" << bag.first << ") " << b.color << ": " << b.count << std::endl;
        }
    }

    std::string find_color = "shiny gold";
    int count = 0;

    // part one
    for (const auto &bag: bags) {
        for (const auto &inside: bag.second) {
            if (find_bag(bags, inside, find_color)) {
                count++;
                break;
            }
        }
    }

    std::cout << count << std::endl;

    // part two
    count = 0;
    for (const auto &b: bags[find_color]) {
        count += count_bags(bags, b);
    }

    std::cout << count << std::endl;

    return 0;
}