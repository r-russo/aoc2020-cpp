#include <iostream>
#include <fstream>
#include <vector>

int part1(const std::vector<std::string> &instructions, const std::vector<int> &arguments);
int part2(const std::vector<std::string> &instructions, const std::vector<int> &arguments);

int main() {
    std::ifstream f;
    f.open("../input_08.txt");
    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    std::string substr;
    std::vector<std::string> instructions;
    std::vector<int> arguments;
    while (std::getline(f, line)) {
        substr = line.substr(0, 3);
        instructions.push_back(substr);
        substr = line.substr(4, std::string::npos);
        arguments.push_back((int) std::stol(substr));
    }

    f.close();
    
    std::cout << part1(instructions, arguments) << std::endl;
    std::cout << part2(instructions, arguments) << std::endl;

    return 0;
}

int part1(const std::vector<std::string> &instructions, const std::vector<int> &arguments) {
    int a = 0;
    int pc = 0;

    std::vector<int> visited(instructions.size(), 0);

    while (pc < instructions.size()) {
        if (visited[pc]++ > 0) {
            break;
        }
        if (instructions[pc] == "acc") {
            a += arguments[pc];
        } else if (instructions[pc] == "jmp") {
            pc += arguments[pc];
            continue;
        }
        pc++;
    }

    return a;
}

int part2(const std::vector<std::string> &instructions, const std::vector<int> &arguments) {
    for (int i = 0; i < instructions.size(); ++i) {
        if (instructions[i] != "jmp") {
            continue;
        }

        int a = 0;
        int pc = 0;
        bool infinite_loop = false;
        std::vector<int> visited(instructions.size(), 0);

        while (pc < instructions.size()) {
            if (pc == i) {
                pc++;
                continue;
            }

            if (visited[pc]++ > 0) {
                infinite_loop = true;
                break;
            }
            if (instructions[pc] == "acc") {
                a += arguments[pc];
            } else if (instructions[pc] == "jmp") {
                pc += arguments[pc];
                continue;
            }
            pc++;
        }

        if (!infinite_loop) {
            return a;
        }
    }

    return 0;
}