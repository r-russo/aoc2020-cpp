#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

uint64_t part1(const std::vector<uint64_t> &nums, int preamble_len);
uint64_t part2(const std::vector<uint64_t> &nums, uint64_t invalid_number);

int main() {
    std::ifstream f;
    f.open("../input_09.txt");
    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    std::vector<uint64_t> nums;
    while (std::getline(f, line)) {
        nums.push_back(std::stol(line));
    }

    f.close();

    uint64_t invalid_number = part1(nums, 25);

    std::cout << invalid_number << std::endl;
    std::cout << part2(nums, invalid_number) << std::endl;

    return 0;
}

uint64_t part1(const std::vector<uint64_t> &nums, int preamble_len) {
    std::vector<uint64_t> prev_nums(preamble_len);

    for (int i = 0; i < nums.size() - preamble_len - 1; ++i) {
        for (int j = 0; j < preamble_len; ++j) {
            prev_nums[j] = nums[i + j];
        }

        uint64_t next_num = nums[i + preamble_len];

        bool found_sum = false;
            for (int ix2 = ix1 + 1; ix2 < prev_nums.size(); ++ix2) {
                if (prev_nums[ix1] + prev_nums[ix2] == next_num) {
                    found_sum = true;
                    break;
                }
            }
        }

        if (!found_sum) {
            return next_num;
        }
    }

    return 0;
}

uint64_t part2(const std::vector<uint64_t> &nums, uint64_t invalid_number) {
    uint64_t sum;
    for (int end = 0; end < nums.size(); ++end) {
        for (int start = end - 1; start >= 0; --start) {
            sum = 0;
            for (int ix = start; ix < end; ++ix) {
                sum += nums[ix];

                if (sum > invalid_number) {
                    break;
                } else if (sum == invalid_number) {
                    int min = *std::min_element(nums.begin() + start, nums.begin() + end);
                    int max = *std::max_element(nums.begin() + start, nums.begin() + end);

                    return min + max;
                }
            }
        }
    }

    return 0;
}