#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

uint64_t count_orders(const std::vector<int> &ordered_adapters, size_t start, size_t end);

int part1(const std::vector<int> &adapters);
uint64_t part2(const std::vector<int> &adapters);

int main() {
    std::ifstream f;

    f.open("../input_10.txt");
    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    std::vector<int> adapters;

    while (std::getline(f, line)) {
        adapters.push_back((int) std::stol(line));
    }

    f.close();

    std::sort(adapters.begin(), adapters.end());
    std::cout << part1(adapters) << std::endl;
    std::cout << part2(adapters) << std::endl;

    return 0;
}

uint64_t count_orders(const std::vector<int> &ordered_adapters, size_t start, size_t end) {
    uint64_t count = 1;
//    for (size_t i = start + 1; i < end; ++i) {
//        if (ordered_adapters[i + 1] - ordered_adapters[i - 1] <= 3) {
//            count++;
//        }
//        for (size_t j = i + 1; j < end; ++j) {
//            if (ordered_adapters[i + j + 1] - ordered_adapters[i - 1] <= 3) {
//                count++;
//            }
//        }
//    }

    if (end - start == 1) {
        return 1;
    } else if (end - start == 2) {
        return 2;
    } else if (end - start == 3) {
        return 4;
    } else if (end - start == 4) {
        return 7;
    } else if (end - start == 5) {
        return 13;
    }

    return count;
}

int part1(const std::vector<int> &adapters) {
    int sum_dif_3 = 1;
    int sum_dif_1 = 0;

    int last_value = 0;
    for (const int &a: adapters) {
        int dif = a - last_value;
        if (dif == 1) {
            sum_dif_1++;
        } else if (dif == 3) {
            sum_dif_3++;
        }
        last_value = a;
    }

    return sum_dif_3 * sum_dif_1;
}

uint64_t part2(const std::vector<int> &adapters) {
    std::vector<int> order;

    order.push_back(0);
    order.insert(order.end(), adapters.begin(), adapters.end());
    order.push_back(order.back() + 3);

    size_t start = 0;
    uint64_t count = 1;
    for (int i = 1; i < order.size(); ++i) {
        if (order[i] - order[i - 1] >= 3) {
            count *= count_orders(order, start, i - 1);
            start = i;
        }
    }
    return count;
}
