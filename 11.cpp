#include <fstream>
#include <iostream>
#include <string>
#include <vector>

int count_adjacent(const std::vector<std::string> &seats, int row, int col) {
    int count{0};

    for (int r = row - 1; r <= row + 1; ++r) {
        if (r < 0 || r >= seats.size())
            continue;
        for (int c = col - 1; c <= col + 1; ++c) {
            if (c < 0 || c >= seats[r].size())
                continue;
            if (c == col && r == row)
                continue;
            if (seats[r][c] == '#') {
                count++;
            }
        }
    }

    return count;
}

int count_closest(const std::vector<std::string> &seats, int row, int col) {
    int count{0};

    for (int dirx = -1; dirx <= 1; ++dirx) {
        for (int diry = -1; diry <= 1; ++diry) {
            if (dirx == 0 && diry == 0)
                continue;

            int current_row = row + diry;
            int current_col = col + dirx;

            while (current_row < seats.size() && current_row >= 0 && current_col < seats[current_row].size() &&
                   current_col >= 0) {

                if (seats[current_row][current_col] == '#') {
                    count++;
                    break;
                } else if (seats[current_row][current_col] == 'L') {
                    break;
                }

                current_row += diry;
                current_col += dirx;
            }
        }
    }

    return count;
}

uint32_t part1(const std::vector<std::string> &starting_seats);
uint32_t part2(std::vector<std::string> &starting_seats);

int main() {
    std::ifstream f;

    f.open("../input_11.txt");

    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    std::vector<std::string> seats;

    while (std::getline(f, line)) {
        seats.push_back(line);
    }

    f.close();

    std::cout << part1(seats) << std::endl;
    std::cout << part2(seats) << std::endl;

    return 0;
}

uint32_t part1(const std::vector<std::string> &starting_seats) {
    auto seats = starting_seats;
    std::vector<std::string> new_seats(seats.size());

    while (1) {
        for (size_t row = 0; row < seats.size(); ++row) {
            std::string line;
            for (size_t col = 0; col < seats[row].size(); ++col) {
                int n_occupied = count_adjacent(seats, row, col);
                if (seats[row][col] == 'L' && n_occupied == 0) {
                    line += '#';
                } else if (seats[row][col] == '#' && n_occupied >= 4) {
                    line += 'L';
                } else {
                    line += seats[row][col];
                }
            }
            new_seats[row] = line;
        }

        if (seats == new_seats) {
            break;
        }
        seats = new_seats;
    }

    uint32_t result{};

    for (const auto &r : new_seats) {
        for (const char &c : r) {
            if (c == '#')
                result++;
        }
    }

    return result;
}

uint32_t part2(std::vector<std::string> &starting_seats) {
    auto seats = starting_seats;
    std::vector<std::string> new_seats(seats.size());

    while (1) {
        for (size_t row = 0; row < seats.size(); ++row) {
            std::string line;
            for (size_t col = 0; col < seats[row].size(); ++col) {
                int n_occupied = count_closest(seats, row, col);
                if (seats[row][col] == 'L' && n_occupied == 0) {
                    line += '#';
                } else if (seats[row][col] == '#' && n_occupied >= 5) {
                    line += 'L';
                } else {
                    line += seats[row][col];
                }
            }
            new_seats[row] = line;
        }

        if (seats == new_seats) {
            break;
        }
        seats = new_seats;
    }

    uint32_t result{};

    for (const auto &r : new_seats) {
        for (const char &c : r) {
            if (c == '#')
                result++;
        }
    }

    return result;
}
