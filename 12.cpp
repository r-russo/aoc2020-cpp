#include <fstream>
#include <iostream>
#include <string>
#include <vector>

typedef struct Point {
    long x{};
    long y{};
} Point;

uint32_t manhattan_distance(const Point &pos) { return abs(pos.x) + abs(pos.y); }

int dir_to_deg(const Point &dir) {
    if (dir.x == 1 && dir.y == 0) {
        return 0;
    } else if (dir.x == 0 && dir.y == 1) {
        return 90;
    } else if (dir.x == -1 && dir.y == 0) {
        return 180;
    } else if (dir.x == 0 && dir.y == -1) {
        return 270;
    } else {
        return -1;
    }
}

Point deg_to_dir(int dir) {
    if (dir < 0) {
        dir = 360 + dir;
    }

    dir %= 360;

    if (dir == 0) {
        return Point{1, 0};
    } else if (dir == 90) {
        return Point{0, 1};
    } else if (dir == 180) {
        return Point{-1, 0};
    } else if (dir == 270) {
        return Point{0, -1};
    } else {
        return Point{0, 0};
    }
}

Point rotate(const Point &pos, int deg) {
    Point new_pos{};

    deg %= 360;

    if (deg == 90 || deg == 270) {
        new_pos.x = -pos.y;
        new_pos.y = pos.x;

        if (deg == 270) {
            new_pos.x = -new_pos.x;
            new_pos.y = -new_pos.y;
        }
    } else if (deg == 180) {
        new_pos.x = -pos.x;
        new_pos.y = -pos.y;
    }

    return new_pos;
}

uint32_t part1(const std::vector<std::string> &directions);
uint32_t part2(const std::vector<std::string> &directions);

int main() {
    std::ifstream f;
    f.open("../input_12.txt");

    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    std::vector<std::string> directions;
    while (std::getline(f, line)) {
        if (!line.empty()) {
            directions.push_back(line);
        }
    }

    f.close();

    std::cout << part1(directions) << std::endl;
    std::cout << part2(directions) << std::endl;

    return 0;
}

uint32_t part1(const std::vector<std::string> &directions) {
    Point pos{};
    Point direction{1, 0};

    for (const auto &d : directions) {
        char action = d[0];
        int arg = static_cast<int>(std::stol(d.substr(1)));

        switch (action) {
        case 'F':
            pos.x += arg * direction.x;
            pos.y += arg * direction.y;
            break;
        case 'N':
            pos.y += arg;
            break;
        case 'S':
            pos.y -= arg;
            break;
        case 'E':
            pos.x += arg;
            break;
        case 'W':
            pos.x -= arg;
            break;
        case 'L':
            direction = deg_to_dir(dir_to_deg(direction) + arg);
            break;
        case 'R':
            direction = deg_to_dir(dir_to_deg(direction) - arg);
            break;
        }
    }

    return manhattan_distance(pos);
}

uint32_t part2(const std::vector<std::string> &directions) {
    Point ship_pos{};
    Point waypoint_pos{10, 1};

    Point new_dir{};
    for (const auto &d : directions) {
        char action = d[0];
        int arg = static_cast<int>(std::stol(d.substr(1)));

        switch (action) {
        case 'F':
            ship_pos.x += waypoint_pos.x * arg;
            ship_pos.y += waypoint_pos.y * arg;
            break;
        case 'N':
            waypoint_pos.y += arg;
            break;
        case 'S':
            waypoint_pos.y -= arg;
            break;
        case 'E':
            waypoint_pos.x += arg;
            break;
        case 'W':
            waypoint_pos.x -= arg;
            break;
        case 'L':
            waypoint_pos = rotate(waypoint_pos, arg);
            break;
        case 'R':
            waypoint_pos = rotate(waypoint_pos, 360 - arg);
            break;
        }
    }

    return manhattan_distance(ship_pos);
}
