#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

uint32_t part1(const std::vector<int> &buses, uint32_t starting_timestamp);
uint64_t part2(const std::vector<int> &buses, const std::vector<int> &offsets);

int main() {
    std::ifstream f;
    f.open("../input_13.txt");

    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    int line_number{};

    uint32_t starting_timestamp{};
    std::vector<int> buses;
    std::vector<int> offsets;
    int offset{};
    while (std::getline(f, line)) {
        line_number++;

        if (line_number == 1) {
            starting_timestamp = static_cast<int>(std::stol(line));
        } else if (line_number == 2) {
            size_t find_from{};
            size_t found_at{};
            while ((found_at = line.find(',', find_from)) != std::string::npos) {
                std::string num = line.substr(find_from, found_at - find_from);
                if (num != "x") {
                    buses.push_back(static_cast<int>(std::stol(num)));
                    offsets.push_back(offset);
                }
                find_from = found_at + 1;
                offset++;
            }
            std::string num = line.substr(find_from, found_at - find_from);
            if (num != "x") {
                buses.push_back(static_cast<int>(std::stol(num)));
                offsets.push_back(offset);
            }
        }
    }
    f.close();

    std::cout << part1(buses, starting_timestamp) << std::endl;
    std::cout << part2(buses, offsets) << std::endl;

    return 0;
}

uint32_t part1(const std::vector<int> &buses, uint32_t starting_timestamp) {
    uint32_t min{};
    uint32_t result{};
    for (const auto &b : buses) {
        uint32_t current_timestamp{starting_timestamp};
        bool departed = current_timestamp % b == 0;
        while (!departed) {
            current_timestamp++;
            departed = current_timestamp % b == 0;
        }

        if (min == 0) {
            min = current_timestamp;
            result = (min - starting_timestamp) * b;
        } else if (current_timestamp < min) {
            min = std::min(min, current_timestamp);
            result = (min - starting_timestamp) * b;
        }
    }

    return result;
}

uint64_t part2(const std::vector<int> &buses, const std::vector<int> &offsets) {
    uint64_t timestamp{};
    uint64_t distance = static_cast<uint64_t>(buses[0]);

    for (size_t i = 1; i < buses.size(); ++i) {
        while (1) {
            timestamp += distance;
            if ((timestamp + offsets[i]) % buses[i] == 0) {
                distance *= buses[i];
                break;
            }
        }
    }

    return timestamp;
}
