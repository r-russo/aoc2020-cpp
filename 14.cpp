#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <bitset>

uint64_t part1(const char input_file[]);
uint64_t part2(const char input_file[]);

int main() {
    std::cout << part1("../input_14.txt") << std::endl;
    std::cout << part2("../input_14.txt") << std::endl;
    return 0;
}

uint64_t part1(const char input_file[]) {
    std::ifstream f;

    f.open(input_file);

    std::string line;
    uint64_t mask_zeros = 1;
    uint64_t mask_ones = 0;
    std::map<size_t, uint64_t> memory;
    while (std::getline(f, line)) {
        size_t equal = line.find('=');
        std::string var = line.substr(0, equal - 1);
        std::string arg = line.substr(equal + 2, std::string::npos);

        if (var.find("mem") != std::string::npos) {
            size_t mem_pos = std::stol(line.substr(4, line.find(']', 4)));
            uint64_t arg_num = std::stol(arg);

            arg_num |= mask_ones;
            arg_num &= mask_zeros;

            memory[mem_pos] = arg_num;
        } else if (var.find("mask") != std::string::npos) {
            mask_ones = 0;
            mask_zeros = 0;
            for (int i = 0; i < arg.size(); ++i) {
                if (arg[i] == '0') {
                    mask_zeros |= (1UL << (35 - i));
                } else if (arg[i] == '1') {
                    mask_ones |= (1UL << (35 - i));
                }
            }
            mask_zeros = ~mask_zeros;
        }
    }
    f.close();

    uint64_t sum = 0;
    for (const auto &m: memory) {
        sum += m.second;
    }

    return sum;
}

uint64_t part2(const char input_file[]) {
    std::ifstream f;

    f.open(input_file);

    std::string line;
    uint64_t mask_ones = 0;
    std::vector<size_t> float_pos;
    std::map<size_t, uint64_t> memory;
    while (std::getline(f, line)) {
        size_t equal = line.find('=');
        std::string var = line.substr(0, equal - 1);
        std::string arg = line.substr(equal + 2, std::string::npos);

        if (var.find("mem") != std::string::npos) {
            size_t mem_pos = std::stol(line.substr(4, line.find(']', 4)));
            uint64_t arg_num = std::stol(arg);

            mem_pos |= mask_ones;

            if (float_pos.empty()) {
                memory[mem_pos] = arg_num;
            } else {
                for (uint64_t i = 0; i < 1UL << float_pos.size(); ++i) {
                    size_t new_mempos = mem_pos;
                    for (uint64_t s = 0; s < float_pos.size(); ++s) {
                        if (((i >> s) & 1UL) == 0) {
                            new_mempos &= ~(1UL << float_pos[s]);
                        } else {
                            new_mempos |= 1UL << float_pos[s];
                        }
                    }
                    memory[new_mempos] = arg_num;
                }
            }
        } else if (var.find("mask") != std::string::npos) {
            mask_ones = 0;
            float_pos.clear();
            for (int i = 0; i < arg.size(); ++i) {
                if (arg[i] == '0') {
                } else if (arg[i] == '1') {
                    mask_ones |= (1UL << (35 - i));
                } else {
                    float_pos.push_back(35 - i);
                }
            }
        }
    }
    f.close();

    uint64_t sum = 0;
    for (const auto &m: memory) {
        sum += m.second;
    }

    return sum;
}
