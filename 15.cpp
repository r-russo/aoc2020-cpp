#include <iostream>
#include <vector>
#include <unordered_map>

uint32_t find_n_spoken_number(const std::vector<uint32_t> &initial_numbers, uint32_t n);

int main() {
//    std::string input = "0,3,6";
    std::string input = "6,4,12,1,20,0,16";

    std::vector<uint32_t> starting_numbers;
    size_t find_pos;
    size_t last_pos = 0;
    do {
        find_pos = input.find(',', last_pos);
        starting_numbers.push_back(std::stol(input.substr(last_pos, last_pos - find_pos)));

        last_pos = find_pos + 1;
    } while (find_pos != std::string::npos);

    std::cout << find_n_spoken_number(starting_numbers, 2020) << std::endl;
    std::cout << find_n_spoken_number(starting_numbers, 30'000'000) << std::endl;

    return 0;
}

uint32_t find_n_spoken_number(const std::vector<uint32_t> &initial_numbers, uint32_t n) {
    std::vector<uint32_t> spoken_numbers(n);
    std::unordered_map<uint32_t, size_t> existing_numbers;

    size_t current_ix = 0;
    uint32_t next;

    while (current_ix < n) {
//        std::cout << current_ix << std::endl;

        if (current_ix < initial_numbers.size()) {
            spoken_numbers[current_ix] = initial_numbers[current_ix];
            existing_numbers.insert(std::pair<uint32_t, size_t>(initial_numbers[current_ix], current_ix));
            current_ix++;
            continue;
        }

        auto it = existing_numbers.find(spoken_numbers[current_ix - 1]);
        if (it == existing_numbers.end()) {
            next = 0;
            existing_numbers.insert(std::pair<uint32_t, size_t>(spoken_numbers[current_ix - 1], current_ix - 1));
        } else {
            next = current_ix - existing_numbers[spoken_numbers[current_ix - 1]] - 1;
            existing_numbers[spoken_numbers[current_ix - 1]] = current_ix - 1;
        }
        spoken_numbers[current_ix++] = next;
    }

    return next;
}
