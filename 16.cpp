#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <numeric>

int part1(std::vector<std::vector<int>> &tickets, const std::vector<std::vector<std::string>> &fields);
uint64_t part2(const std::vector<std::vector<int>> &tickets,
               const std::vector<std::vector<std::string>> &fields,
               const std::vector<std::string> &descriptions);
bool get_rules_indexes(const std::vector<std::vector<int>> &correct_rules,
                       const std::vector<int> &ordered_indexes,
                       std::vector<int> &result, size_t last_ix);

int main() {
    std::ifstream f;
    f.open("../input_16.txt");

    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    std::vector<std::vector<std::string>> fields;
    std::vector<std::string> description;
    std::vector<std::vector<int>> tickets;
    int block = 0;

    while (std::getline(f, line)) {
        if (line.empty()) {
            block++;
            continue;
        }

        size_t find_pos, next_pos;
        std::vector<std::string> ranges;
        std::vector<int> nums;
        switch (block) {
            case 0: // header
                find_pos = line.find(':');
                description.push_back(line.substr(0, find_pos));

                ranges.clear();
                find_pos--;
                do {
                    next_pos = line.find("or", find_pos + 1);
                    ranges.push_back(line.substr(find_pos + 3, next_pos - find_pos - 4));
                    find_pos = next_pos;
                } while (find_pos != std::string::npos);

                fields.push_back(ranges);
                break;
            case 1: // your ticket
            case 2: // nearby tickets
                if (std::isdigit(line[0])) {
                    nums.clear();
                    find_pos = -1;
                    do {
                        next_pos = line.find(',', ++find_pos);
                        nums.push_back(std::stol(line.substr(find_pos, find_pos - next_pos)));
                        find_pos = next_pos;
                    } while (find_pos != std::string::npos);
                    tickets.push_back(nums);
                }
                break;
            default:break;
        }
    }

    std::cout << part1(tickets, fields) << std::endl;
    std::cout << part2(tickets, fields, description) << std::endl;

    return 0;
}

int part1(std::vector<std::vector<int>> &tickets, const std::vector<std::vector<std::string>> &fields) {
    int scanning_error_rate = 0;

    std::vector<std::vector<int>> ranges;

    for (const auto &field : fields) {
        for (const auto &range : field) {
            size_t find_pos = range.find('-');
            int min = (int) std::stol(range.substr(0, find_pos));
            int max = (int) std::stol(range.substr(find_pos + 1, std::string::npos));

            std::vector<int> minmax = {min, max};

            ranges.push_back(minmax);
        }
    }

    std::vector<int> invalid_ix;

    for (int i = 0; i < tickets.size(); ++i) {
        for (const auto &n: tickets[i]) {
            bool valid = false;
            for (const auto &range: ranges) {
                if (n >= range[0] && n <= range[1]) {
                    valid = true;
                    break;
                }
            }

            if (!valid) {
                scanning_error_rate += n;
                invalid_ix.push_back(i);
            }
        }
    }

    std::vector<std::vector<int>> new_tickets;
    for (int i = 0; i < tickets.size(); ++i) {
        bool found = false;
        for (const auto &ix: invalid_ix) {
            if (i == ix) {
                found = true;
                break;
            }
        }

        if (!found) {
            new_tickets.push_back(tickets[i]);
        }
    }

    tickets = new_tickets;

    return scanning_error_rate;
}

uint64_t part2(const std::vector<std::vector<int>> &tickets,
               const std::vector<std::vector<std::string>> &fields,
               const std::vector<std::string> &descriptions) {
    std::vector<std::vector<std::vector<int>>> ranges;
    for (const auto &field : fields) {
        std::vector<std::vector<int>> current_ranges;
        for (const auto &range: field) {
            size_t find_pos = range.find('-');
            int min = (int) std::stol(range.substr(0, find_pos));
            int max = (int) std::stol(range.substr(find_pos + 1, std::string::npos));

            std::vector<int> minmax = {min, max};

            current_ranges.push_back(minmax);
        }
        ranges.push_back(current_ranges);
    }

    std::vector<std::vector<int>> correct_rules;
    std::vector<int> rules;
    for (int col = 0; col < tickets[0].size(); ++col) {
        rules.clear();
        for (int r = 0; r < ranges.size(); ++r) {
            bool valid = false;
            for (const auto &t: tickets) {
                valid = false;
                for (const auto &range: ranges[r]) {
                    if (t[col] >= range[0] && t[col] <= range[1]) {
                        valid = true;
                    }
                }
                if (!valid) {
                    break;
                }
            }

            if (valid) {
                rules.push_back(r);
            }
        }
        correct_rules.push_back(rules);
    }

    std::vector<int> indexes_rules(correct_rules.size());
    std::iota(indexes_rules.begin(), indexes_rules.end(), 0);
    std::sort(indexes_rules.begin(), indexes_rules.end(),
              [&correct_rules](size_t i1, size_t i2) -> bool {
                  return correct_rules[i1].size() < correct_rules[i2].size();
              });

    std::vector<int> result;
    get_rules_indexes(correct_rules, indexes_rules, result, 0);

    uint64_t prod = 1;
    for (int i = 0; i < descriptions.size(); ++i) {
        if (descriptions[result[i]].find("departure") != std::string::npos) {
            prod *= tickets[0][indexes_rules[i]];
        }
    }

    return prod;
}

bool get_rules_indexes(const std::vector<std::vector<int>> &correct_rules,
                       const std::vector<int> &ordered_indexes,
                       std::vector<int> &result, size_t last_ix) {
    if (last_ix == correct_rules.size()) {
        return true;
    }
    bool pushed = false;
    for (const auto &r : correct_rules[ordered_indexes[last_ix]]) {
        if (std::find(result.begin(), result.end(), r) == result.end()) {
            result.push_back(r);
            pushed = true;

            if (!get_rules_indexes(correct_rules, ordered_indexes, result, ++last_ix)) {
                result.pop_back();
            } else {
                break;
            }
        }
    }

    if (!pushed) {
        return false;
    }

    return true;
}