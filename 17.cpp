#include <iostream>
#include <fstream>
#include <vector>
#include <string>

void grow_states(std::vector<std::vector<std::vector<bool>>> &states);
void grow_states(std::vector<std::vector<std::vector<std::vector<bool>>>> &states);
int count_neighbors(const std::vector<std::vector<std::vector<bool>>> &states, int slice, int row, int col);
int count_neighbors(const std::vector<std::vector<std::vector<std::vector<bool>>>> &states,
                    int hslice, int slice, int row, int col);
std::vector<std::vector<std::vector<bool>>> simulate(const std::vector<std::vector<std::vector<bool>>> &prev_states);
std::vector<std::vector<std::vector<std::vector<bool>>>> simulate(
        const std::vector<std::vector<std::vector<std::vector<bool>>>> &prev_states);
int part1(const std::vector<std::vector<std::vector<bool>>> &initial_states);
int part2(const std::vector<std::vector<std::vector<std::vector<bool>>>> &initial_states);

int main() {
    std::ifstream f;
    f.open("../input_17.txt");
    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    std::vector<std::vector<std::vector<bool>>> states; // [slice][row][col]
    std::vector<std::vector<bool>> slice;
    while (std::getline(f, line)) {
        std::vector<bool> row;
        for (const char &c: line) {
            row.push_back(c == '#');
        }
        slice.push_back(row);
    }

    states.push_back(slice);

    std::vector<std::vector<std::vector<std::vector<bool>>>> states2;
    states2.push_back(states);

    std::cout << part1(states) << std::endl;
    std::cout << part2(states2) << std::endl;

    return 0;
}

int count_neighbors(const std::vector<std::vector<std::vector<bool>>> &states, int slice, int row, int col) {
    int c {};
    for (int z = -1; z <= 1; ++z) {
        for (int y = -1; y <= 1; ++y) {
            for (int x = -1; x <= 1; ++x) {
                if (x == 0 && y == 0 && z == 0) {
                    continue;
                }

                if (slice + z < states.size() && slice + z >= 0 &&
                        row + y < states[slice].size() && row + y >= 0 &&
                        col + x < states[slice][row].size() && col + x >= 0) {
                    c += (int) states[slice + z][row + y][col + x];
                }
            }
        }
    }

    return c;
}

int count_neighbors(const std::vector<std::vector<std::vector<std::vector<bool>>>> &states,
                    int hslice, int slice, int row, int col) {
    int c {};
    for (int w = -1; w <= 1; ++w) {
        for (int z = -1; z <= 1; ++z) {
            for (int y = -1; y <= 1; ++y) {
                for (int x = -1; x <= 1; ++x) {
                    if (x == 0 && y == 0 && z == 0 && w == 0) {
                        continue;
                    }

                    if (hslice + w < states.size() && hslice + w >= 0 &&
                            slice + z < states[hslice].size() && slice + z >= 0 &&
                            row + y < states[hslice][slice].size() && row + y >= 0 &&
                            col + x < states[hslice][slice][row].size() && col + x >= 0) {
                        c += (int) states[hslice + w][slice + z][row + y][col + x];
                    }
                }
            }
        }
    }

    return c;
}

void grow_states(std::vector<std::vector<std::vector<bool>>> &states) {
    for (auto &slice: states) {
        for (auto &row: slice) {
            row.push_back(false);
            row.insert(row.begin(), false);
        }
        slice.emplace_back(slice[0].size(), false);
        slice.emplace(slice.begin(), slice[0].size(), false);
    }

    states.emplace_back(states[0].size(), std::vector<bool>(states[0].size(), false));
    states.emplace(states.begin(), states[0].size(), std::vector<bool>(states[0].size(), false));
}

void grow_states(std::vector<std::vector<std::vector<std::vector<bool>>>> &states) {
    for (auto &hslice: states) {
        grow_states(hslice);
//        for (auto &slice: hslice) {
//            for (auto &row: slice) {
//                row.push_back(false);
//                row.insert(row.begin(), false);
//            }
//            slice.emplace_back(slice[0].size(), false);
//            slice.emplace(slice.begin(), slice[0].size(), false);
//        }
//        hslice.emplace_back(hslice[0].size(), std::vector<bool>(hslice[0].size(), false));
//        hslice.emplace(hslice.begin(), hslice[0].size(), std::vector<bool>(hslice[0].size(), false));
    }

    std::vector<std::vector<bool>> empty_slice;
    empty_slice.reserve(states[0][0].size());
    for (int i = 0; i < states[0][0].size(); ++i) {
        empty_slice.emplace_back(states[0][0].size(), false);
    }

    states.emplace_back(states[0].size(), empty_slice);
    states.emplace(states.begin(), states[0].size(), empty_slice);
}

std::vector<std::vector<std::vector<bool>>> simulate(const std::vector<std::vector<std::vector<bool>>> &prev_states) {
    std::vector<std::vector<std::vector<bool>>> new_states = prev_states;
    for (int slice = 0; slice < prev_states.size(); ++slice) {
        for (int row = 0; row < prev_states[slice].size(); ++row) {
            for (int col = 0; col < prev_states[slice][row].size(); ++col) {
                int active_neighbors = count_neighbors(prev_states, slice, row, col);

                if (prev_states[slice][row][col] && !(active_neighbors == 2 || active_neighbors == 3)) {
                    new_states[slice][row][col] = false;
                } else if (!prev_states[slice][row][col] && active_neighbors == 3) {
                    new_states[slice][row][col] = true;
                }
            }
        }
    }

    return new_states;
}

std::vector<std::vector<std::vector<std::vector<bool>>>> simulate(
        const std::vector<std::vector<std::vector<std::vector<bool>>>> &prev_states) {
    std::vector<std::vector<std::vector<std::vector<bool>>>> new_states = prev_states;
    for (int hslice = 0; hslice < prev_states.size(); ++hslice) {
        for (int slice = 0; slice < prev_states[hslice].size(); ++slice) {
            for (int row = 0; row < prev_states[hslice][slice].size(); ++row) {
                for (int col = 0; col < prev_states[hslice][slice][row].size(); ++col) {
                    int active_neighbors = count_neighbors(prev_states, hslice, slice, row, col);

                    if (prev_states[hslice][slice][row][col] && !(active_neighbors == 2 || active_neighbors == 3)) {
                        new_states[hslice][slice][row][col] = false;
                    } else if (!prev_states[hslice][slice][row][col] && active_neighbors == 3) {
                        new_states[hslice][slice][row][col] = true;
                    }
                }
            }
        }
    }

    return new_states;
}

int part1(const std::vector<std::vector<std::vector<bool>>> &initial_states) {
    auto prev_states = initial_states;
    for (int cycles = 0; cycles < 6; ++cycles) {
        grow_states(prev_states);
        auto new_states = simulate(prev_states);
        prev_states = new_states;
    }

    int count_active_cubes {};

    for (const auto &slice : prev_states) {
        for (const auto &row : slice) {
            for (const auto &col : row) {
                count_active_cubes += (int) col;
            }
        }
    }

    return count_active_cubes;
}

int part2(const std::vector<std::vector<std::vector<std::vector<bool>>>> &initial_states) {
    auto prev_states = initial_states;
    for (int cycles = 0; cycles < 6; ++cycles) {
        grow_states(prev_states);
        auto new_states = simulate(prev_states);
        prev_states = new_states;
    }

    int count_active_cubes {};

    for (const auto &hslice: prev_states) {
        for (const auto &slice : hslice) {
            for (const auto &row : slice) {
                for (const auto &col : row) {
                    count_active_cubes += (int) col;
                }
            }
        }
    }

    return count_active_cubes;
}