#include <iostream>
#include <fstream>
#include <string>
#include <vector>

uint64_t parse_operations(const std::string &operation);
uint64_t parse_operations2(const std::string &operation);
uint64_t part1(const std::vector<std::string> &operations);
uint64_t part2(const std::vector<std::string> &operations);

int main() {
    std::ifstream f;
    f.open("../input_18.txt");
    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    std::vector<std::string> operations;

    while (std::getline(f, line)) {
        operations.push_back(line);
    }

    f.close();

    std::cout << part1(operations) << std::endl;
    std::cout << part2(operations) << std::endl;

    return 0;
}

uint64_t parse_operations(const std::string &operation) {
    uint64_t number = 0;
    uint64_t result = 0;
    std::string paren_buffer;
    char last_op = '+';
    int parens = 0;

    for (const char &c: operation) {
        if (parens == 0) {
            if (c == '+' || c == '*') {
                if (last_op == '+') {
                    result += number;
                } else if (last_op == '*') {
                    result *= number;
                }
                last_op = c;
                number = 0;
            } else if (c == '(') {
                parens++;
            } else if (isdigit(c)) {
                number = number * 10 + (c - '0');
            }
        } else {
            paren_buffer.push_back(c);
            if (c == '(') {
                parens++;
            } else if (c == ')') {
                parens--;
            }

            if (parens == 0) {
                paren_buffer.pop_back();
                number = parse_operations(paren_buffer);
                paren_buffer.clear();
            }
        }
    }

    if (last_op == '+') {
        result += number;
    } else if (last_op == '*') {
        result *= number;
    }

    return result;
}

uint64_t parse_operations2(const std::string &operation) {
    uint64_t number = 0;
    uint64_t result = 0;
    std::string paren_buffer;
    int parens = 0;

    std::string first_pass;
    for (const char &c: operation) {
        if (parens == 0) {
            if (c == '+') {
                result += number;
                number = 0;
            } else if (c == '*') {
                result += number;
                first_pass += std::to_string(result);
                first_pass.push_back(c);
                result = 0;
                number = 0;
            } else if (c == '(') {
                parens++;
            } else if (isdigit(c)) {
                number = number * 10 + (c - '0');
            }
        } else {
            paren_buffer.push_back(c);
            if (c == '(') {
                parens++;
            } else if (c == ')') {
                parens--;
            }

            if (parens == 0) {
                paren_buffer.pop_back();
                number = parse_operations2(paren_buffer);
                paren_buffer.clear();
            }
        }
    }

    result += number;
    first_pass += std::to_string(result);

    result = 1;
    number = 0;
    for (const char &c: first_pass) {
        if (parens == 0) {
            if (c == '*') {
                result *= number;
                number = 0;
            } else if (c == '(') {
                parens++;
            } else if (isdigit(c)) {
                number = number * 10 + (c - '0');
            }
        } else {
            paren_buffer.push_back(c);
            if (c == '(') {
                parens++;
            } else if (c == ')') {
                parens--;
            }

            if (parens == 0) {
                paren_buffer.pop_back();
                number = parse_operations2(paren_buffer);
                paren_buffer.clear();
            }
        }
    }

    result *= number;

    return result;
}

uint64_t part1(const std::vector<std::string> &operations) {
    uint64_t result = 0;
    for (const auto &op: operations) {
        result += parse_operations(op);
    }

    return result;
}

uint64_t part2(const std::vector<std::string> &operations) {
    uint64_t result = 0;
    for (const auto &op: operations) {
        result += parse_operations2(op);
    }

    return result;
}
