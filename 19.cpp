#include <iostream>
#include <fstream>
#include <string>
#include <unordered_map>
#include <vector>
#include <regex>

typedef std::unordered_map<int, std::string> RuleSet;

std::string get_rule(const RuleSet &rules, int rule);
std::string get_rule_with_loops(const RuleSet &rules, int rule);
int part1(const RuleSet &rules, const std::vector<std::string> &messages);
int part2(const RuleSet &rules, const std::vector<std::string> &messages);

int main() {
    std::ifstream f;

    f.open("../input_19.txt");

    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    RuleSet rules;
    std::vector<std::string> messages;

    bool reading_rules = true;

    while (std::getline(f, line)) {
        if (line.empty()) {
            reading_rules = false;
        }

        if (reading_rules) {
            size_t sep = line.find(':');
            int num = (int) std::stol(line.substr(0, sep));
            std::string rule = line.substr(sep + 2);
            rules.insert(std::pair<int, std::string>(num, rule));
        } else {
            messages.push_back(line);
        }
    }

    std::cout << part1(rules, messages) << std::endl;
    std::cout << part2(rules, messages) << std::endl;

    return 0;
}

std::string get_rule(const RuleSet &rules, int rule) {
    std::string current_rule;
    int num = 0;

    current_rule.push_back('(');
    for (const char &c: rules.at(rule)) {
        if (c == ' ') {
            if (num == 0) {
                continue;
            }
            current_rule += get_rule(rules, num);
            num = 0;
        } else if (c == '|') {
            current_rule.push_back(c);
            num = 0;
        } else if (isdigit(c)) {
            num *= 10;
            num += c - '0';
        } else if (isalpha(c)) {
            current_rule.push_back(c);
        }
    }
    if (num != 0) {
        current_rule += get_rule(rules, num);
    }

    current_rule.push_back(')');

    if (current_rule.size() == 3) {
        return current_rule.substr(1, 1);
    }

    return current_rule;
}

std::string get_rule_with_loops(const RuleSet &rules, int rule) {
    std::string current_rule;
    int num = 0;

    current_rule.push_back('(');
    for (const char &c: rules.at(rule)) {
        if (c == ' ') {
            if (num == 0) {
                continue;
            }
            if (num == rule) {
                if (rule == 8) {
                    current_rule += get_rule_with_loops(rules, 42) + "+";
                } else if (rule == 11) {
                    current_rule += "(" + get_rule_with_loops(rules, 42) +
                            get_rule_with_loops(rules, 31) + ")" + "+";
                }
            } else {
                current_rule += get_rule_with_loops(rules, num);
            }
            num = 0;
        } else if (c == '|') {
            current_rule.push_back(c);
            num = 0;
        } else if (isdigit(c)) {
            num *= 10;
            num += c - '0';
        } else if (isalpha(c)) {
            current_rule.push_back(c);
        }
    }
    if (num != 0) {
        if (num == rule) {
            current_rule += "+";
        } else {
            current_rule += get_rule_with_loops(rules, num);
        }
    }

    current_rule.push_back(')');

    if (current_rule.size() == 3) {
        return current_rule.substr(1, 1);
    }

    return current_rule;
}

int part1(const RuleSet &rules, const std::vector<std::string> &messages) {
    std::regex rules_regex("^" + get_rule(rules, 0) + "$");
    int count = 0;

    for (const std::string &m: messages) {
        if (std::regex_match(m, rules_regex)) {
            count++;
        }
    }

    return count;
}

int part2(const RuleSet &rules, const std::vector<std::string> &messages) {
    RuleSet modified_rules = rules;
//    modified_rules.at(11) += " | 42 11 31";

    for (int i = 1; i < 6; ++i) {
        modified_rules.at(8) += " |";
        for (int j = 0; j <= i; ++j) {
            modified_rules.at(8) += " 42";
        }

        modified_rules.at(11) += " |";
        for (int j = 0; j <= i; ++j) {
            modified_rules.at(11) += " 42";
        }
        for (int j = 0; j <= i; ++j) {
            modified_rules.at(11) += " 31";
        }
    }

    std::regex rules_regex("^" + get_rule(modified_rules, 0) + "$");
    int count = 0;

    for (const std::string &m: messages) {
        if (std::regex_match(m, rules_regex)) {
            count++;
        }
    }

    return count;
}
