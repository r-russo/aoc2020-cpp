#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
#include "20_tile.h"

uint64_t part1(std::vector<Tile> &tile_set);
int part2(std::vector<Tile> &tile_set);

int main() {
    std::ifstream f;

    f.open("../input_20.txt");
    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    std::vector<Tile> tile_set;
    Tile tile;
    while (std::getline(f, line)) {
        if (line.empty()) {
            tile_set.push_back(tile);
            tile = Tile();
        } else {
            if (line[0] == 'T') {
                tile.set_id(static_cast<uint16_t>(std::stol(line.substr(5))));
            } else {
                tile.append_row(line);
            }
        }
    }
    tile_set.push_back(tile);
    f.close();

    std::cout << part1(tile_set) << std::endl;
    std::cout << part2(tile_set) << std::endl;

    return 0;
}

uint64_t part1(std::vector<Tile> &tile_set) {
    uint64_t result {1};

    // find corners
    for (size_t t1 = 0; t1 < tile_set.size(); ++t1) {
        int count_matches {};
        for (size_t t2 = 0; t2 < tile_set.size(); ++t2) {
            if (t1 == t2) continue;

            if (tile_set[t1].match(tile_set[t2]) >= 0) {
                count_matches++;
                tile_set[t1].add_neighbour(t2);
            }
        }

        if (count_matches == 2) {  // corner
            tile_set[t1].set_corner();
            result *= tile_set[t1].get_id();
        }
    }

    return result;
}

int part2(std::vector<Tile> &tile_set) {
    std::vector<Tile> ordered_tile_set(tile_set.size());

    size_t starting_ix {};
    for (size_t i = 0; i < tile_set.size(); ++i) {
        if (tile_set[i].is_corner()) {
            starting_ix = i;
            break;
        }
    }

    ordered_tile_set[0] = tile_set[starting_ix];

    // find orientation for which the starting tile is located TOP LEFT
    for (int r = 0; r < 4; ++r) {
        ordered_tile_set[0].rotate();
        for (int f = 0; f < 3; ++f) {
            if (f == 1) {
                ordered_tile_set[0].flip();
            } else if (f == 2) {
                ordered_tile_set[0].flip();
                ordered_tile_set[0].flip(false);
            }

            auto neighbours = ordered_tile_set[0].get_neighbours();
            if ((ordered_tile_set[0].match(tile_set[neighbours[0]]) == RIGHT &&
                    ordered_tile_set[0].match(tile_set[neighbours[1]]) == BOTTOM) ||
                    (ordered_tile_set[0].match(tile_set[neighbours[1]]) == RIGHT &&
                            ordered_tile_set[0].match(tile_set[neighbours[0]]) == BOTTOM)) {
                break;
            }
        }
    }

    auto row_size = static_cast<size_t>(sqrt(ordered_tile_set.size()));

    for (size_t pos = 0; pos < ordered_tile_set.size(); ++pos) {
        for (const auto &n: ordered_tile_set[pos].get_neighbours()) {
            int matching_position = ordered_tile_set[pos].match(tile_set[n]);
            size_t new_pos {};

            switch (matching_position) {
                case TOP:
                    new_pos = pos - row_size;
                    break;
                case RIGHT:
                    new_pos = pos + 1;
                    break;
                case BOTTOM:
                    new_pos = pos + row_size;
                    break;
                case LEFT:
                    new_pos = pos - 1;
                    break;
                default:
                    break;
            }

            if (ordered_tile_set[new_pos].get_id() == 0) {
                ordered_tile_set[new_pos] = tile_set[n];
            }
        }
    }

    std::vector<std::string> ordered_image(row_size * 8);
    for (size_t i = 0; i < ordered_tile_set.size(); ++i) {
        size_t current_row = i / row_size * 8;
        for (const auto &l: ordered_tile_set[i].get_image()) {
            ordered_image[current_row] += l;
            current_row++;
        }
    }

    // find the pattern
    const std::vector<std::string> pattern = {
            "                  # ",
            "#    ##    ##    ###",
            " #  #  #  #  #  #   "
    };

    const int num_hashes {15};

    int count_monsters {};

    for (int r = 0; r < 4; ++r) {
        bool counted {};
        std::vector<std::string> adjusted_image(ordered_image.size());
        for (int i = static_cast<int>(ordered_image.size()) - 1; i >= 0; --i) {
            size_t col = 0;
            for (const char &c: ordered_image[i]) {
                adjusted_image[col++] += c;
            }
        }
        ordered_image = adjusted_image;

        for (int f = 0; f < 3; ++f) {
            if (f == 1 || f == 2) {
                size_t row = 0;
                for (int i = static_cast<int>(ordered_image.size()) - 1; i >= 0; --i) {
                    adjusted_image[row++] = ordered_image[i];
                }
            }
            if (f == 2) {
                for (int i = 0; i < ordered_image.size(); ++i) {
                    adjusted_image[i] = std::string(ordered_image[i].rbegin(), ordered_image[i].rend());
                }
            }

            ordered_image = adjusted_image;

            count_monsters = 0;
            for (size_t row = 0; row < ordered_image.size() - pattern.size(); ++row) {
                for (size_t col = 0; col < ordered_image[row].size() - pattern[0].size(); ++col) {
                    bool found_match {true};
                    for (size_t i = 0; i < pattern.size(); ++i) {
                        for (size_t j = 0; j < pattern[i].size(); ++j) {
                            if (pattern[i][j] == '#' && ordered_image[row + i][col + j] != '#') {
                                found_match = false;
                                break;
                            }
                        }
                        if (!found_match) break;
                    }

                    if (found_match) count_monsters++;
                }
            }
            if (count_monsters > 0) {
                counted = true;
                break;
            }
        }
        if (counted) break;
    }

    int result {};

    // count hashes
    for (const auto &l: ordered_image) {
        for (const auto &c: l) {
            if (c == '#') result++;
        }
    }

    result -= count_monsters * num_hashes;

    return result;
}
