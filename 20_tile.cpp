#include "20_tile.h"

Tile::Tile() = default;

void Tile::rotate(bool cw) {
    std::vector<std::string> new_image(image.size());
    if (cw) {
        for (int i = static_cast<int>(image.size()) - 1; i >= 0; --i) {
            size_t col = 0;
            for (const char &c: image[i]) {
                new_image[col++] += c;
            }
        }
    } else {
        for (auto &l: image) {
            size_t col = 0;
            for (int i = static_cast<int>(l.size()) - 1; i >= 0; --i) {
                new_image[col++] += l[i];
            }
        }
    }

    image = new_image;
}

void Tile::flip(bool v) {
    std::vector<std::string> new_image(image.size());
    if (v) {
        size_t row = 0;
        for (int i = static_cast<int>(image.size()) - 1; i >= 0; --i) {
            new_image[row++] = image[i];
        }
    } else {
        for (int i = 0; i < image.size(); ++i) {
            new_image[i] = std::string(image[i].rbegin(), image[i].rend());
        }
    }

    image = new_image;
}

void Tile::append_row(const std::string &row) {
    image.push_back(row);
}

int Tile::match(Tile &t) const {
    for (int f = 0; f < 3; ++f) {
        if (f == 1) {
            t.flip();
        } else if (f == 2) {
            t.flip();
            t.flip(false);
        }
        for (int r = 0; r < 4; ++r) {
            int matching_side {-1};
            for (int side = 0; side < 4; ++side) {
                if (get_edge(side) == t.get_edge((side + 2) % 4)) {
                    matching_side = side;
                    break;
                }
            }
            if (matching_side >= 0) {
                return matching_side;
            }
            t.rotate();
        }
    }

    return -1;
}

std::string Tile::get_edge(int side) const {
    std::string edge;
    switch (side) {
        case TOP:
            return image[0];
        case RIGHT:
            for (const std::string &line: image) {
                edge += line.back();
            }
            return edge;
        case BOTTOM:
            return image[image.size() - 1];
        case LEFT:
            for (const std::string &line: image) {
                edge += line[0];
            }
            return edge;
        default:
            return "";
    }
}

std::vector<std::string> Tile::get_image() const {
    std::vector<std::string> new_image(image.size() - 2);
    for (size_t i = 1; i < image.size() - 1; ++i) {
        new_image[i - 1] = image[i].substr(1, image[i].size() - 2);
    }

    return new_image;
}

std::ostream &operator<<(std::ostream &out, const Tile &t) {
    out << t.id << std::endl;
    for (const auto &l : t.image) {
        out << l << std::endl;
    }

    return out;
}

