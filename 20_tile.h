#ifndef __20_TILE_H_
#include <iostream>
#include <string>
#include <vector>

enum Sides {
    TOP, RIGHT, BOTTOM, LEFT
};

class Tile {
public:
    Tile();
    void rotate(bool cw = true);
    void flip(bool v = true);
    void append_row(const std::string &row);
    int match(Tile &t) const;
    std::string get_edge(int side) const;
    std::vector<std::string> get_image() const;

    friend std::ostream &operator << (std::ostream &out, const Tile &t);

    int get_id() const { return id; };
    void set_id(int new_id) { id = new_id; };
    bool is_corner() const { return corner; };
    void set_corner() { corner = true; };
    std::vector<size_t> get_neighbours() const { return neighbours; };
    void add_neighbour(size_t ix) { neighbours.push_back(ix); };
private:
    std::vector<std::string> image {};
    int id {};
    bool corner {};
    std::vector<size_t> neighbours {};
};

#endif