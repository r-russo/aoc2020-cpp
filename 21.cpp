#include <algorithm>
#include <fstream>
#include <iostream>
#include <numeric>
#include <string>
#include <unordered_map>
#include <vector>

std::unordered_map<std::string, std::string>
generate_unordered_map_ingredients(const std::vector<std::vector<std::string>> &food_ingredients,
                                   const std::vector<std::vector<std::string>> &food_allergens) {
    std::vector<std::string> list_of_allergens;
    for (const auto &f : food_allergens) {
        for (const auto &allergen : f) {
            if (std::find(list_of_allergens.begin(), list_of_allergens.end(), allergen) == list_of_allergens.end()) {
                list_of_allergens.push_back(allergen);
            }
        }
    }
    std::unordered_map<std::string, std::string> allergen_name;
    for (const std::string &allergen : list_of_allergens) {
        std::vector<size_t> matches;
        for (size_t i = 0; i < food_allergens.size(); ++i) {
            if (std::find(food_allergens[i].begin(), food_allergens[i].end(), allergen) != food_allergens[i].end()) {
                matches.push_back(i);
                continue;
            }
        }

        for (const std::string &ingredient : food_ingredients[matches[0]]) {
            if (allergen_name.find(ingredient) != allergen_name.end()) {
                continue;
            }
            if (matches.size() > 1) {
                bool found_ingredient{true};
                for (size_t i = 1; i < matches.size(); ++i) {
                    if (std::find(food_ingredients[matches[i]].begin(), food_ingredients[matches[i]].end(),
                                  ingredient) == food_ingredients[matches[i]].end()) {
                        found_ingredient = false;
                        break;
                    }
                }
                if (found_ingredient) {
                    allergen_name.emplace(std::pair<std::string, std::string>(ingredient, allergen));
                    break;
                }
            } else {
                // FIXME: check at end?
                allergen_name.emplace(std::pair<std::string, std::string>(ingredient, allergen));
            }
        }
    }
    return allergen_name;
}

uint16_t part1(const std::vector<std::vector<std::string>> &food_ingredients,
               const std::vector<std::vector<std::string>> &food_allergens,
               const std::unordered_map<std::string, std::string> &allergen_name);

std::string part2(const std::vector<std::vector<std::string>> &food_ingredients,
                  const std::vector<std::vector<std::string>> &food_allergens,
                  const std::unordered_map<std::string, std::string> &allergen_name);

int main() {
    std::ifstream f;
    f.open("../input_21.txt");

    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    std::vector<std::vector<std::string>> food_ingredients;
    std::vector<std::vector<std::string>> food_allergens;
    while (std::getline(f, line)) {
        size_t found_at{}, find_from{};
        bool is_allergen{};
        std::vector<std::string> ingredients;
        std::vector<std::string> allergens;

        while ((found_at = line.find(' ', find_from)) != std::string::npos) {
            if (!is_allergen) {
                ingredients.push_back(line.substr(find_from, found_at - find_from));
            } else {
                allergens.push_back(line.substr(find_from, found_at - find_from - 1));
            }

            find_from = found_at + 1;
            if (line[find_from] == '(') {
                is_allergen = true;
                find_from += 10;
            }
        }

        if (is_allergen) {
            allergens.push_back(line.substr(find_from));
            allergens.back().erase(allergens.back().size() - 1);
        } else {
            ingredients.push_back(line.substr(find_from));
        }

        food_ingredients.push_back(ingredients);
        food_allergens.push_back(allergens);
    }

    f.close();

    auto allergen_name = generate_unordered_map_ingredients(food_ingredients, food_allergens);

    std::cout << part1(food_ingredients, food_allergens, allergen_name) << std::endl;
    std::cout << part2(food_ingredients, food_allergens, allergen_name) << std::endl;

    return 0;
}

uint16_t part1(const std::vector<std::vector<std::string>> &food_ingredients,
               const std::vector<std::vector<std::string>> &food_allergens,
               const std::unordered_map<std::string, std::string> &allergen_name) {
    uint32_t count{};
    for (const auto &f : food_ingredients) {
        for (const auto &i : f) {
            if (allergen_name.find(i) == allergen_name.end()) {
                count++;
            }
        }
    }

    return count;
}

std::string part2(const std::vector<std::vector<std::string>> &food_ingredients,
                  const std::vector<std::vector<std::string>> &food_allergens,
                  const std::unordered_map<std::string, std::string> &allergen_name) {
    std::vector<std::string> allergens, ingredients;
    for (const auto &e : allergen_name) {
        ingredients.emplace_back(e.first);
        allergens.emplace_back(e.second);
    }

    std::vector<size_t> idx(ingredients.size());
    std::iota(idx.begin(), idx.end(), 0);
    std::sort(idx.begin(), idx.end(), [&allergens](size_t i1, size_t i2) { return allergens[i1] < allergens[i2]; });

    std::string result;
    for (const auto &e : idx) {
        result += ingredients[e];
        result += ',';
    }
    result.pop_back();

    return result;
}
