#include <deque>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

void print_game(const std::deque<int> &p1, const std::deque<int> &p2) {
    std::cout << "Player 1: ";
    for (const auto &c : p1) {
        std::cout << c << " ";
    }
    std::cout << std::endl;

    std::cout << "Player 2: ";
    for (const auto &c : p2) {
        std::cout << c << " ";
    }
    std::cout << std::endl;
}

uint32_t get_score(const std::deque<int> &player) {
    uint32_t score{};
    for (size_t i = 0; i < player.size(); ++i) {
        score += player[i] * (player.size() - i);
    }

    return score;
}

uint32_t part1(std::deque<int> p1, std::deque<int> p2);
uint32_t part2(std::deque<int> p1, std::deque<int> p2);

int main() {
    std::ifstream f;
    f.open("../input_22.txt");
    if (!f.is_open()) {
        return 1;
    }

    std::string line;
    std::deque<int> p1;
    std::deque<int> p2;
    char player{};
    while (std::getline(f, line)) {
        if (line.empty()) {
            continue;
        }

        if (line[0] == 'P') {
            player = line[7];
        } else {
            if (player == '1') {
                p1.emplace_back(static_cast<int>(std::stol(line)));
            } else if (player == '2') {
                p2.emplace_back(static_cast<int>(std::stol(line)));
            }
        }
    }

    f.close();

    std::cout << part1(p1, p2) << std::endl;
    std::cout << part2(p1, p2) << std::endl;

    return 0;
}

uint32_t part1(std::deque<int> p1, std::deque<int> p2) {
    while (!p1.empty() && !p2.empty()) {
        if (p1.front() > p2.front()) {
            p1.push_back(p1.front());
            p1.push_back(p2.front());
        } else {
            p2.push_back(p2.front());
            p2.push_back(p1.front());
        }

        p1.pop_front();
        p2.pop_front();
    }

    if (p1.empty()) {
        return get_score(p2);
    } else {
        return get_score(p1);
    }
}

bool check_match(const std::deque<int> &player, const std::vector<std::deque<int>> &hands) {
    for (const auto &h : hands) {
        if (h == player) {
            return true;
        }
    }

    return false;
}

int recursive_combat(std::deque<int> p1, std::deque<int> p2, int &score, int game = 1) {
    std::vector<std::deque<int>> hands_p1;
    std::vector<std::deque<int>> hands_p2;
    while (!p1.empty() && !p2.empty()) {
        int round_winner = 0;
        // check last rounds for same configuration
        if (check_match(p1, hands_p1) || check_match(p2, hands_p2)) {
            score = get_score(p1);
            return 1;
        }

        hands_p1.push_back(p1);
        hands_p2.push_back(p2);

        // draw
        auto p1_draw = p1.front();
        auto p2_draw = p2.front();
        p1.pop_front();
        p2.pop_front();

        // check if p1 and p2 has at least p1_draw and p2_draw cards
        if (p1_draw <= p1.size() && p2_draw <= p2.size()) {
            // recursive combat
            int score{};
            auto new_p1 = p1;
            new_p1.resize(p1_draw);
            auto new_p2 = p2;
            new_p2.resize(p2_draw);
            round_winner = recursive_combat(std::move(new_p1), std::move(new_p2), score, game + 1);
        } else {
            // regular combat
            round_winner = p1_draw > p2_draw ? 1 : 2;
        }

        // update decks
        if (round_winner == 1) {
            p1.push_back(p1_draw);
            p1.push_back(p2_draw);
        } else if (round_winner == 2) {
            p2.push_back(p2_draw);
            p2.push_back(p1_draw);
        }
    }

    if (p1.empty()) {
        score = get_score(p2);
        return 2;
    } else {
        score = get_score(p1);
        return 1;
    }
}

uint32_t part2(std::deque<int> p1, std::deque<int> p2) {
    int score{};

    recursive_combat(p1, p2, score);

    return score;
}
