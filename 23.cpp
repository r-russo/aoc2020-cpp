#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

void print_state(const std::vector<uint64_t> &next_cup, size_t cup) {
    size_t current_cup{cup};
    std::cout << current_cup + 1 << " ";
    for (size_t i = 1; i < next_cup.size(); ++i) {
        current_cup = next_cup[current_cup];
        std::cout << current_cup + 1 << " ";
    }
    std::cout << std::endl;
}

void simulate(std::vector<uint64_t> &next_cup, size_t starting_cup, int moves) {
    size_t cup{starting_cup};
    std::vector<uint64_t> pickup(3);
    for (int i = 0; i < moves; i++) {
        pickup[0] = next_cup[cup];
        for (size_t j = 1; j < 3; ++j) {
            pickup[j] = next_cup[pickup[j - 1]];
        }
        int destination = cup;
        do {
            destination--;
            if (destination < 0) {
                destination += next_cup.size();
            }
        } while (std::find(pickup.begin(), pickup.end(), destination) != pickup.end());

        /* std::cout << "Move " << i + 1 << "/" << moves << std::endl; */
        /* print_state(next_cup, cup); */
        /* std::cout << "Pickup:"; */
        /* for (const auto &p : pickup) { */
        /*     std::cout << " " << p + 1; */
        /* } */
        /* std::cout << std::endl; */
        /* std::cout << "Destination: " << destination + 1 << std::endl; */
        /* std::cout << std::endl; */

        next_cup[cup] = next_cup[pickup.back()];
        size_t next = next_cup[destination];
        next_cup[destination] = pickup[0];
        next_cup[pickup.back()] = next;

        cup = next_cup[cup];
    }
}

std::string part1(std::string cups, uint64_t moves);
uint64_t part2(std::string cups);

int main() {
    std::string input{"389125467"};
    /* std::string input{"463528179"}; */

    std::cout << part1(input, 100) << std::endl;
    std::cout << part2(input) << std::endl;
    return 0;
}

std::string part1(std::string cups, uint64_t moves) {
    std::vector<uint64_t> next_cup(cups.size());
    for (size_t i = 0; i < cups.size(); ++i) {
        size_t cup;
        if (i == 0) {
            cup = static_cast<size_t>(cups[cups.size() - 1] - '0') - 1;
        } else {
            cup = static_cast<size_t>(cups[i - 1] - '0') - 1;
        }
        uint64_t next = static_cast<uint64_t>(cups[i] - '0') - 1;
        next_cup[cup] = next;
    }
    simulate(next_cup, cups[0] - '0' - 1, moves);
    std::string out;
    out.push_back(next_cup[0] + '0' + 1);
    for (int i = 0; i < cups.size() - 2; ++i) {
        size_t ix = static_cast<size_t>(out.back() - '0' - 1);
        out.push_back(next_cup[ix] + '0' + 1);
    }

    return out;
}

uint64_t part2(std::string cups) {
    std::vector<uint64_t> next_cup(cups.size());
    for (size_t i = 0; i < cups.size(); ++i) {
        size_t cup;
        if (i == 0) {
            cup = static_cast<size_t>(cups[cups.size() - 1] - '0') - 1;
        } else {
            cup = static_cast<size_t>(cups[i - 1] - '0') - 1;
        }
        uint64_t next = static_cast<uint64_t>(cups[i] - '0') - 1;
        next_cup[cup] = next;
    }

    // complete array
    next_cup[cups.back() - '0' - 1] = next_cup.size();
    for (uint64_t i = cups.size(); i < 1'000'000 - 1; ++i) {
        next_cup.push_back(i + 1);
    }
    next_cup.push_back(cups[0] - '0' - 1);

    simulate(next_cup, cups[0] - '0' - 1, 10'000'000);
    unsigned long long result = next_cup[0] * next_cup[next_cup[0]];
    std::cout << next_cup[0] + 1 << " " << next_cup[next_cup[0]] + 1 << std::endl;
    return result;
}
