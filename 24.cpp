#include <algorithm>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

typedef struct Coordinate {
    int x{}, y{};
} Coordinate;

void print_grid(const std::vector<std::vector<int>> &grid) {
    for (int i = 0; i < grid.size(); ++i) {
        for (int j = 0; j < grid[i].size(); ++j) {
            if (i % 2 == 1) {
                std::cout << " " << grid[i][j] % 2;
            } else {
                std::cout << grid[i][j] % 2 << " ";
            }
        }
        std::cout << std::endl;
    }
}

void grow_grid(std::vector<std::vector<int>> &grid) {
    for (auto &row : grid) {
        row.insert(row.begin(), 0);
        row.push_back(0);
    }
    grid.insert(grid.begin(), std::vector<int>(grid[0].size(), 0));
    grid.insert(grid.begin(), std::vector<int>(grid[0].size(), 0));
    grid.push_back(std::vector<int>(grid[0].size()));
    grid.push_back(std::vector<int>(grid[0].size()));
}

void move(std::string dir, Coordinate &coord) {
    bool oddr = abs(coord.y) % 2;

    if (dir == "e") {
        coord.x++;
    } else if (dir == "w") {
        coord.x--;
    } else if (dir == "nw") {
        coord.y--;
        if (!oddr) {
            coord.x--;
        }
    } else if (dir == "ne") {
        coord.y--;
        if (oddr) {
            coord.x++;
        }
    } else if (dir == "sw") {
        coord.y++;
        if (!oddr) {
            coord.x--;
        }
    } else if (dir == "se") {
        coord.y++;
        if (oddr) {
            coord.x++;
        }
    }
}

int count_black_tiles(const std::vector<std::vector<int>> &grid) {
    int count{};
    for (const auto &row : grid) {
        for (const auto &col : row) {
            if (col % 2 == 1)
                count++;
        }
    }
    return count;
}

bool is_black(const std::vector<std::vector<int>> &grid, int x, int y) {
    if (x >= 0 && x < grid[y].size() && y >= 0 && y < grid.size()) {
        return grid[y][x] % 2 == 1;
    }

    return false;
}

int count_black_neighbours(const std::vector<std::vector<int>> &grid, int x, int y) {
    int neighbours{};

    if (y % 2 == 0) {
        neighbours += is_black(grid, x + 1, y);
        neighbours += is_black(grid, x, y - 1);
        neighbours += is_black(grid, x - 1, y - 1);
        neighbours += is_black(grid, x - 1, y);
        neighbours += is_black(grid, x - 1, y + 1);
        neighbours += is_black(grid, x, y + 1);
    } else {
        neighbours += is_black(grid, x + 1, y);
        neighbours += is_black(grid, x + 1, y - 1);
        neighbours += is_black(grid, x, y - 1);
        neighbours += is_black(grid, x - 1, y);
        neighbours += is_black(grid, x, y + 1);
        neighbours += is_black(grid, x + 1, y + 1);
    }

    return neighbours;
}

uint32_t part1(const std::vector<std::vector<int>> &grid) { return count_black_tiles(grid); };
uint32_t part2(std::vector<std::vector<int>> grid);

int main() {
    std::ifstream f;
    f.open("../input_24.txt");
    if (!f.is_open())
        return 1;

    std::string line;
    std::vector<std::string> relative_movement;

    while (std::getline(f, line)) {
        if (!line.empty()) {
            relative_movement.emplace_back(line);
        }
    }

    f.close();

    std::vector<Coordinate> coordinates(relative_movement.size());

    for (int i = 0; i < relative_movement.size(); ++i) {
        std::string dir{};
        for (const char &c : relative_movement[i]) {
            dir += c;
            if (c == 'e' || c == 'w') {
                move(dir, coordinates[i]);
                dir.clear();
            }
        }
    }

    int min_x = std::min_element(coordinates.begin(), coordinates.end(), [&](const Coordinate &a, const Coordinate &b) {
                    return a.x < b.x;
                })->x;
    int max_x = std::max_element(coordinates.begin(), coordinates.end(), [&](const Coordinate &a, const Coordinate &b) {
                    return a.x < b.x;
                })->x;
    int min_y = std::min_element(coordinates.begin(), coordinates.end(), [&](const Coordinate &a, const Coordinate &b) {
                    return a.y < b.y;
                })->y;
    int max_y = std::max_element(coordinates.begin(), coordinates.end(), [&](const Coordinate &a, const Coordinate &b) {
                    return a.y < b.y;
                })->y;

    bool add_row = abs(min_y) % 2 == 1;

    std::vector<std::vector<int>> grid;
    grid.resize(max_y - min_y + 1 + add_row);
    for (auto &r : grid) {
        r.resize(max_x - min_x + 1);
    }

    for (const Coordinate &c : coordinates) {
        grid[c.y - min_y + add_row][c.x - min_x]++;
    }

    std::cout << part1(grid) << std::endl;
    std::cout << part2(grid) << std::endl;

    return 0;
}

uint32_t part2(std::vector<std::vector<int>> grid) {
    for (int i = 0; i < 100; ++i) {
        grow_grid(grid);
        auto new_grid = grid;

        for (int y = 0; y < grid.size(); ++y) {
            for (int x = 0; x < grid[y].size(); ++x) {
                int neighbours = count_black_neighbours(grid, x, y);
                if (is_black(grid, x, y)) {
                    if (neighbours == 0 || neighbours > 2) {
                        new_grid[y][x]++;
                    }
                } else {
                    if (neighbours == 2) {
                        new_grid[y][x]++;
                    }
                }
            }
        }

        grid = new_grid;
    }

    return count_black_tiles(grid);
}
