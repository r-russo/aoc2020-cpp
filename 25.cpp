#include <iostream>

const int starting_subject_number = 7;

int get_loop_size(uint32_t key) {
    int loop_size{};
    uint64_t value{1};
    while (1) {
        loop_size++;
        value *= starting_subject_number;
        value %= 20201227;

        if (value == key) {
            break;
        }
    }
    return loop_size;
}

uint32_t get_encryption_key(uint32_t key, int loop_size) {
    uint64_t value{1};
    for (int i = 0; i < loop_size; ++i) {
        value *= key;
        value %= 20201227;
    }

    return value;
}

uint32_t part1(uint32_t card_public_key, uint32_t door_public_key);

int main() {
    /* uint32_t card_public_key = 5764801; */
    /* uint32_t door_public_key = 17807724; */

    uint32_t card_public_key = 1717001;
    uint32_t door_public_key = 523731;

    std::cout << part1(card_public_key, door_public_key) << std::endl;

    return 0;
}

uint32_t part1(uint32_t card_public_key, uint32_t door_public_key) {
    return get_encryption_key(card_public_key, get_loop_size(door_public_key));
}
